use crate::{run_part, scan};

const INPUT: &str = include_str!("../inputs/input_05");
const NUM_COLUMNS: usize = 9;

#[derive(Debug)]
struct Crates {
    piles: [Vec<char>; NUM_COLUMNS],
}

impl Crates {
    fn new(input: &[&str]) -> Self {
        let mut piles: [Vec<char>; NUM_COLUMNS] = Default::default();
        for line in input.iter().rev().skip(1) {
            line.chars()
                .skip(1)
                .step_by(4)
                .zip(piles.iter_mut())
                .for_each(|(c, vec)| {
                    if c != ' ' {
                        vec.push(c);
                    }
                });
        }
        Self { piles }
    }
}

#[derive(Debug)]
struct ParseLineError(String);

fn parse_line(line: &str) -> Result<(usize, usize, usize), ParseLineError> {
    let filtered = line
        .chars()
        .filter(|c| c.is_digit(10) || c.is_whitespace())
        .collect::<String>();
    let a = scan!(filtered.trim(), "  ", usize, usize, usize);
    match a {
        (Some(a), Some(b), Some(c)) => Ok((a, b - 1, c - 1)),
        _ => Err(ParseLineError(line.to_string())),
    }
}

pub fn part_01() {
    let crates_lines = INPUT
        .lines()
        .take_while(|line| !line.is_empty())
        .collect::<Vec<_>>();
    let mut crates = Crates::new(&crates_lines);

    run_part!(
        INPUT.lines().skip_while(|line| !line.is_empty()).skip(1),
        |line| -> Result<(), ParseLineError> {
            let (num_to_move, from, to) = parse_line(line)?;

            for _ in 0..num_to_move {
                let val = crates.piles[from].pop().unwrap();
                crates.piles[to].push(val);
            }

            Ok(())
        },
        |_| {
            let fin: String = crates.piles.iter().map(|v| v.last().unwrap()).collect();
            println!("Part 1 final state {fin}");
        }
    );
}

pub fn part_02() {
    let crates_lines = INPUT
        .lines()
        .take_while(|line| !line.is_empty())
        .collect::<Vec<_>>();
    let mut crates = Crates::new(&crates_lines);

    let mut buf = vec![];

    run_part!(
        INPUT.lines().skip_while(|line| !line.is_empty()).skip(1),
        |line| -> Result<(), ParseLineError> {
            let (num_to_move, from, to) = parse_line(line)?;

            for _ in 0..num_to_move {
                buf.push(crates.piles[from].pop().unwrap());
            }

            buf.reverse();
            buf.drain(..).for_each(|val| crates.piles[to].push(val));

            Ok(())
        },
        |_| {
            let fin: String = crates.piles.iter().map(|v| v.last().unwrap()).collect();
            println!("Part 2 final state {fin}");
        }
    );
}
