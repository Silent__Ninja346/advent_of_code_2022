use std::str::FromStr;

use crate::{run_part, scan};

const INPUT: &str = include_str!("../inputs/input_04");

#[derive(Debug)]
struct Elf {
    start: u32,
    end: u32,
}

#[derive(Debug)]
struct ParseElfError(String);

impl FromStr for Elf {
    type Err = ParseElfError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match scan!(s, '-', u32, u32) {
            (Some(start), Some(end)) => Ok(Elf { start, end }),
            _ => Err(ParseElfError(s.to_string())),
        }
    }
}

impl Elf {
    fn contains_all(&self, other: &Self) -> bool {
        let range = self.start..=self.end;
        (other.start..=other.end).all(|v| range.contains(&v))
    }

    fn contains_any(&self, other: &Self) -> bool {
        let range = self.start..=self.end;
        (other.start..=other.end).any(|v| range.contains(&v))
    }
}

#[derive(Debug)]
struct ParseElfPairError(String);

fn parse_elf_pair(s: &str) -> Result<(Elf, Elf), ParseElfPairError> {
    let mut iter = s.split(',');
    let first = iter.next();
    let second = iter.next();

    let (e1, e2) = match (first, second) {
        (Some(e1), Some(e2)) => (e1, e2),
        _ => return Err(ParseElfPairError(s.to_string())),
    };
    match (e1.parse(), e2.parse()) {
        (Ok(elf1), Ok(elf2)) => Ok((elf1, elf2)),
        (Ok(_), Err(err)) => Err(ParseElfPairError(err.0)),
        (Err(err), Ok(_)) => Err(ParseElfPairError(err.0)),
        (Err(_), Err(_)) => Err(ParseElfPairError(s.to_string())),
    }
}

pub fn part_01() {
    run_part!(INPUT.lines(), parse_elf_pair, |elves: &[(Elf, Elf)]| {
        let num = elves
            .iter()
            .filter(|(e1, e2)| {
                if e1.contains_all(&e2) || e2.contains_all(&e1) {
                    true
                } else {
                    false
                }
            })
            .count();

        println!("Number of fully contained: {num}")
    });
}

pub fn part_02() {
    run_part!(INPUT.lines(), parse_elf_pair, |elves: &[(Elf, Elf)]| {
        let num = elves
            .iter()
            .filter(|(e1, e2)| {
                if e1.contains_any(&e2) || e2.contains_any(&e1) {
                    true
                } else {
                    false
                }
            })
            .count();
        println!("Number of partially contained: {num}")
    });
}
