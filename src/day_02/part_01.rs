use crate::{day_02::INPUT, run_part};

use super::{RoundParseError, RpsMove};

fn parse_round(s: &str) -> Result<i32, RoundParseError> {
    const WIN: i32 = 6;
    const DRAW: i32 = 3;

    let (opponent_move, player_move) = match (s.chars().nth(0), s.chars().nth(2)) {
        (Some(opp), Some(player)) => (opp.into(), player.into()),
        _ => return Err(RoundParseError(s.to_string())),
    };

    let mut player_score = player_move as _;

    match (player_move, opponent_move) {
        (RpsMove::Rock, RpsMove::Rock) => player_score += DRAW,
        (RpsMove::Rock, RpsMove::Paper) => {}
        (RpsMove::Rock, RpsMove::Scissors) => player_score += WIN,

        (RpsMove::Paper, RpsMove::Rock) => player_score += WIN,
        (RpsMove::Paper, RpsMove::Paper) => player_score += DRAW,
        (RpsMove::Paper, RpsMove::Scissors) => {}

        (RpsMove::Scissors, RpsMove::Rock) => {}
        (RpsMove::Scissors, RpsMove::Paper) => player_score += WIN,
        (RpsMove::Scissors, RpsMove::Scissors) => player_score += DRAW,
    }

    Ok(player_score)
}

pub fn part_01() {
    run_part!(INPUT.lines(), parse_round, |rounds: &[i32]| {
        println!("Part 1 sum: {}", rounds.iter().sum::<i32>())
    });
}
