use crate::{day_02::INPUT, run_part};

use super::{RoundParseError, RpsMove};

enum Outcome {
    Win,
    Loss,
    Draw,
}

impl From<char> for Outcome {
    fn from(c: char) -> Self {
        match c {
            'X' => Self::Loss,
            'Y' => Self::Draw,
            'Z' => Self::Win,
            _ => panic!("Received invalid character {c} to convert to RpsOutcome"),
        }
    }
}

fn parse_round(s: &str) -> Result<i32, RoundParseError> {
    const WIN: i32 = 6;
    const DRAW: i32 = 3;

    let (opponent_move, round_outcome): (RpsMove, Outcome) =
        match (s.chars().nth(0), s.chars().nth(2)) {
            (Some(opp), Some(round)) => (opp.into(), round.into()),
            _ => return Err(RoundParseError(s.to_string())),
        };

    let mut player_score = 0;

    match round_outcome {
        Outcome::Win => {
            player_score += WIN;
            player_score += opponent_move.counter() as i32;
        }
        Outcome::Loss => {
            player_score += opponent_move.counter().counter() as i32;
        }
        Outcome::Draw => {
            player_score += DRAW;
            player_score += opponent_move as i32;
        }
    }

    Ok(player_score)
}

pub fn part_02() {
    run_part!(INPUT.lines(), parse_round, |rounds: &[i32]| {
        println!("Part 2 sum: {}", rounds.iter().sum::<i32>())
    });
}
