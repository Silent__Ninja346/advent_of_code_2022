mod part_01;
mod part_02;

use std::fmt::Debug;

pub use part_01::part_01;
pub use part_02::part_02;

const INPUT: &str = include_str!("../../inputs/input_02");

#[derive(Debug, Clone, Copy)]
enum RpsMove {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl From<char> for RpsMove {
    fn from(c: char) -> Self {
        match c {
            'A' | 'X' => RpsMove::Rock,
            'B' | 'Y' => RpsMove::Paper,
            'C' | 'Z' => RpsMove::Scissors,
            _ => panic!("Received invalid character {c} to convert to RpsMove"),
        }
    }
}

impl RpsMove {
    fn counter(&self) -> Self {
        match self {
            RpsMove::Rock => Self::Paper,
            RpsMove::Paper => Self::Scissors,
            RpsMove::Scissors => Self::Rock,
        }
    }
}

#[derive(Debug)]
struct RoundParseError(String);
