use std::collections::HashSet;

use itertools::Itertools;

use crate::run_part;

const INPUT: &str = include_str!("../inputs/input_03");

#[derive(Debug)]
struct ParseRucksackError(String);

fn parse_rucksack(s: &str) -> Result<char, ParseRucksackError> {
    let first_pocket = &s[0..(s.len() / 2)];
    let second_pocket = &s[(s.len() / 2)..];
    for c in first_pocket.chars() {
        if second_pocket.contains(c) {
            return Ok(c);
        }
    }
    Err(ParseRucksackError(s.to_string()))
}

pub fn priority(c: char) -> Option<i32> {
    match c {
        'a'..='z' => Some(c as i32 - 'a' as i32 + 1),
        'A'..='Z' => Some(c as i32 - 'A' as i32 + 1 + 26),
        _ => None,
    }
}

pub fn sum_priorities(chars: &[char]) -> i32 {
    chars
        .iter()
        .map(|c| priority(*c).expect("An alphabetic character"))
        .sum::<i32>()
}

pub fn part_01() {
    run_part!(INPUT.lines(), parse_rucksack, |chars| {
        println!("Part 1 sum: {}", sum_priorities(chars))
    });
}

#[derive(Debug)]
struct ParseBadgeError([String; 3]);

fn parse_badge(elves: [&str; 3]) -> Result<char, ParseBadgeError> {
    let chars1: HashSet<char> = elves[0].chars().collect();
    let chars2: HashSet<char> = elves[1].chars().collect();
    let chars3: HashSet<char> = elves[2].chars().collect();

    let common: HashSet<char> = &(&chars1 & &chars2) & &chars3;
    match common.iter().take(1).next() {
        Some(c) => Ok(*c),
        None => Err(ParseBadgeError([
            elves[0].to_string(),
            elves[1].to_string(),
            elves[2].to_string(),
        ])),
    }
}

pub fn part_02() {
    run_part!(
        INPUT
            .lines()
            .chunks(3)
            .into_iter()
            .map(|mut chunk| [(); 3].map(|_| chunk.next().unwrap())),
        parse_badge,
        |badges| println!("Part 2 sum: {}", sum_priorities(badges))
    );
}
