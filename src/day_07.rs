use std::collections::HashMap;
use std::path::PathBuf;
use std::str::FromStr;

use crate::{run_part, scan};

const INPUT: &str = include_str!("../inputs/input_07");

#[derive(Debug)]
enum Command {
    Ls,
    Cd(PathBuf),
}

impl FromStr for Command {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match scan!(s, char::is_whitespace, String, String, PathBuf) {
            (Some(_), Some(typ), Some(path)) if typ == "cd" => Ok(Command::Cd(path)),
            (Some(_), Some(typ), None) if typ == "ls" => Ok(Command::Ls),
            _ => Err(s.to_string()),
        }
    }
}

#[derive(Debug)]
enum Entry {
    Dir(PathBuf),
    File(usize, PathBuf),
}

impl FromStr for Entry {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (typ, filename) = match scan!(s, char::is_whitespace, String, PathBuf) {
            (Some(t), Some(f)) => (t, f),
            _ => Err(s.to_string())?,
        };
        if typ == "dir" {
            Ok(Entry::Dir(filename))
        } else if let Ok(num) = typ.parse() {
            Ok(Entry::File(num, filename))
        } else {
            Err(s.to_string())
        }
    }
}

#[derive(Debug)]
enum Line {
    Command(Command),
    Entry(Entry),
}

impl FromStr for Line {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with('$') {
            Ok(Line::Command(s.parse()?))
        } else {
            Ok(Line::Entry(s.parse()?))
        }
    }
}

pub fn both_parts() {
    run_part!(INPUT.lines(), str::parse, |lines: &[Line]| {
        let mut cwd = PathBuf::new();
        let mut dir_sizes = HashMap::new();
        let mut total_used = 0;

        for line in lines {
            if let Line::Command(Command::Cd(dir)) = line {
                if *dir == PathBuf::from("..") {
                    cwd.pop();
                } else {
                    cwd.push(dir);
                    if !dir_sizes.contains_key(&cwd) {
                        dir_sizes.insert(cwd.clone(), 0);
                    }
                }
                continue;
            }
            if let Line::Entry(Entry::File(size, _)) = line {
                total_used += size;
                let mut tmp = PathBuf::new();
                for dir in cwd.iter() {
                    tmp.push(dir);
                    dir_sizes.entry(tmp.clone()).and_modify(|val| *val += size);
                }
            }
        }

        let s = dir_sizes
            .values()
            .filter(|val| **val < 100000)
            .sum::<usize>();
        println!("< 100000 sum: {s}");

        let unused_space = 70000000 - total_used;
        let mut large_enough_dirs = dir_sizes
            .iter()
            .filter(|(_, v)| **v + unused_space > 30000000)
            .collect::<Vec<_>>();
        large_enough_dirs.sort_by(|(_, v1), (_, v2)| v1.cmp(v2));
        let m = large_enough_dirs[0];
        println!("dir: {m:?}");
    });
}
