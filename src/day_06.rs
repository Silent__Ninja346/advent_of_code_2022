use std::collections::HashSet;
use std::hash::Hash;

const INPUT: &str = include_str!("../inputs/input_06");

fn has_unique_elements<I: IntoIterator>(iter: I) -> bool
where
    I::Item: Eq + Hash,
{
    let mut unique = HashSet::new();
    iter.into_iter().all(|x| unique.insert(x))
}

pub fn either_part(marker_size: usize) {
    let mut buf = INPUT.chars().take(marker_size).collect::<Vec<_>>();
    let mut processed = marker_size;

    for c in INPUT.chars().skip(marker_size) {
        if has_unique_elements(&buf) {
            println!("start found: {buf:?}\nchars processed: {processed}");
            break;
        }

        buf.rotate_left(1);
        *buf.last_mut().unwrap() = c;
        processed += 1;
    }
}
