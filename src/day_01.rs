use std::num::ParseIntError;

use crate::run_part;

const INPUT: &str = include_str!("../inputs/input_01");

fn parse_elf(s: &str) -> Result<i32, ParseIntError> {
    Ok(s.lines()
        .map(str::parse)
        .collect::<Result<Vec<_>, _>>()?
        .iter()
        .sum())
}

pub fn both_parts() {
    run_part!(INPUT.split("\n\n"), parse_elf, |elves: &[i32]| {
        match elves.iter().max() {
            Some(max) => println!("Maximum amount of calories: {max}"),
            None => eprintln!("Elves was empty?"),
        }

        let mut sorted = elves.to_vec();
        sorted.sort_unstable();

        let top_three_sum = sorted.iter().rev().take(3).sum::<i32>();
        println!("Top three sum: {top_three_sum}");
    });
}
