#[macro_export]
macro_rules! scan {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}

#[macro_export]
macro_rules! run_part {
    ($split:expr, $parse_fn:expr, $computation_fn:expr) => {{
        let parse_result = $split.map($parse_fn).collect::<Result<Vec<_>, _>>();

        let parse_worked = match parse_result {
            Ok(vec) => vec,
            Err(e) => {
                eprintln!("Failed to parse input: {e:?}");
                return;
            }
        };

        $computation_fn(&parse_worked);
    }};
}
