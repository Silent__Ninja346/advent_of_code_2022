use std::collections::{HashSet, VecDeque};
use std::fmt::Display;
use std::str::FromStr;

use crate::scan;

const INPUT: &str = include_str!("../inputs/input_10");

#[derive(Debug, Copy, Clone)]
enum Instruction {
    Addx(i32),
    Noop,
}

#[derive(Debug)]
struct ParseInstructionError;

impl FromStr for Instruction {
    type Err = ParseInstructionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let res = scan!(s, char::is_whitespace, String, i32);
        match res {
            (Some(instruction), Some(v)) => {
                if instruction == "addx" {
                    Ok(Self::Addx(v))
                } else {
                    Err(ParseInstructionError)
                }
            }
            (Some(instruction), None) => {
                if instruction == "noop" {
                    Ok(Self::Noop)
                } else {
                    Err(ParseInstructionError)
                }
            }
            _ => Err(ParseInstructionError),
        }
    }
}

impl Instruction {
    pub fn to_cycles(self) -> i32 {
        match self {
            Instruction::Addx(_) => 2,
            Instruction::Noop => 1,
        }
    }
}

#[derive(Debug)]
struct InstructionState {
    instruction: Instruction,
    cycles_left: i32,
}

impl InstructionState {
    fn new(instruction: Instruction, cycles_left: i32) -> Self {
        Self {
            instruction,
            cycles_left,
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum Pixel {
    On,
    Off,
}

#[derive(Debug)]
struct CpuExecutionState {
    register: i32,
    current_clock_cycle: i32,
    current_instruction: Option<InstructionState>,
    instructions: VecDeque<Instruction>,
    display: Vec<Pixel>,
}

impl CpuExecutionState {
    fn new(instructions_str: &str) -> Self {
        let mut instructions = instructions_str
            .lines()
            .map(str::parse)
            .collect::<Result<VecDeque<Instruction>, ParseInstructionError>>()
            .unwrap();
        let first_instruction = instructions
            .pop_front()
            .map(|i| InstructionState::new(i, i.to_cycles()));

        Self {
            register: 1,
            current_clock_cycle: 0,
            current_instruction: first_instruction,
            instructions,
            display: vec![Pixel::Off; 240],
        }
    }

    fn execute_cycle(&mut self) -> bool {
        if self.current_instruction.is_none() {
            return false;
        }

        let current_pixel = self.current_clock_cycle % 40;

        if self.register == current_pixel
            || self.register == current_pixel - 1
            || self.register == current_pixel + 1
        {
            self.display[self.current_clock_cycle as usize] = Pixel::On
        }

        let InstructionState {
            instruction,
            cycles_left,
        } = self.current_instruction.as_mut().unwrap();
        *cycles_left -= 1;
        if *cycles_left <= 0 {
            match instruction {
                Instruction::Addx(n) => self.register += *n,
                Instruction::Noop => {}
            }
            self.current_instruction = self
                .instructions
                .pop_front()
                .map(|i| InstructionState::new(i, i.to_cycles()));
        }
        self.current_clock_cycle += 1;
        true
    }

    fn current_signal_strength(&self) -> i32 {
        self.register * (self.current_clock_cycle + 1)
    }
}

impl Display for CpuExecutionState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut s = String::with_capacity(250);
        for (idx, pix) in self.display.iter().enumerate() {
            match pix {
                Pixel::On => s += "#",
                Pixel::Off => s += ".",
            }
            if (idx + 1) % 40 == 0 {
                s += "\n";
            }
        }
        write!(f, "{s}")
    }
}

pub fn both_parts() {
    let mut state = CpuExecutionState::new(INPUT);
    let intervals = (20..=220).step_by(40).collect::<HashSet<i32>>();

    let mut total = 0;

    loop {
        if intervals.contains(&state.current_clock_cycle) {
            total += state.current_signal_strength();
        }

        if !state.execute_cycle() {
            break;
        }
    }

    println!("part 10 total: {total}");
    println!("{state}");
}
