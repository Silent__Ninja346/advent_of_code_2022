use std::collections::HashSet;

use crate::day_09::INPUT;

use super::{Movement, Point};

struct Rope2 {
    head: Point,
    tail: Point,
    visited: HashSet<Point>,
}

impl Rope2 {
    fn new() -> Self {
        let start = Point::new(0, 0);
        Self {
            head: start,
            tail: start,
            visited: HashSet::new(),
        }
    }

    fn update(&mut self, movement: Movement) {
        let Movement {
            direction: d,
            repeat: r,
        } = movement;

        let delta_direction = d.to_point();

        for _ in 0..r {
            self.head += delta_direction;

            let diff = self.head - self.tail;

            let (dx, dy) = match (diff.x, diff.y) {
                // overlapping
                (0, 0) => (0, 0),
                // touching up/left/down/right
                (0, 1) | (1, 0) | (0, -1) | (-1, 0) => (0, 0),
                // touching diagonally
                (1, 1) | (1, -1) | (-1, 1) | (-1, -1) => (0, 0),
                // need to move up/left/down/right
                (0, 2) => (0, 1),
                (0, -2) => (0, -1),
                (2, 0) => (1, 0),
                (-2, 0) => (-1, 0),
                // need to move to the right diagonally
                (2, 1) => (1, 1),
                (2, -1) => (1, -1),
                // need to move to the left diagonally
                (-2, 1) => (-1, 1),
                (-2, -1) => (-1, -1),
                // need to move up/down diagonally
                (1, 2) => (1, 1),
                (-1, 2) => (-1, 1),
                (1, -2) => (1, -1),
                (-1, -2) => (-1, -1),
                _ => panic!("unhandled case: tail - head = {diff:?}"),
            };

            self.tail += Point::new(dx, dy);
            self.visited.insert(self.tail);
        }
    }
}

pub fn part_01() {
    let movements = INPUT.lines().map(|line| line.parse::<Movement>().unwrap());
    let mut rope = Rope2::new();
    for movement in movements {
        rope.update(movement);
    }
    let visited_count = rope.visited.len();
    println!("{visited_count}");
}
