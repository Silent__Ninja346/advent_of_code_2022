use std::collections::HashSet;

use crate::day_09::INPUT;

use super::{Movement, Point};

struct Rope10 {
    knots: [Point; 10],
    tail_visited: HashSet<Point>,
}

impl Rope10 {
    fn new() -> Self {
        Self {
            knots: [Point::new(0, 0); 10],
            tail_visited: HashSet::new(),
        }
    }

    fn update(&mut self, movement: Movement) {
        let Movement {
            direction: d,
            repeat: r,
        } = movement;

        let delta_direction = d.to_point();

        for _ in 0..r {
            self.knots[0] += delta_direction;

            for idx in 1..self.knots.len() {
                let first = self.knots[idx - 1];
                let second = &mut self.knots[idx];
                let diff = first - *second;
                // let diff = self.knots[idx - 1] - self.knots[idx];

                let (dx, dy) = match (diff.x, diff.y) {
                    // overlapping
                    (0, 0) => (0, 0),
                    // touching up/left/down/right
                    (0, 1) | (1, 0) | (0, -1) | (-1, 0) => (0, 0),
                    // touching diagonally
                    (1, 1) | (1, -1) | (-1, 1) | (-1, -1) => (0, 0),
                    // need to move up/down/left/right
                    (0, 2) => (0, 1),
                    (0, -2) => (0, -1),
                    (2, 0) => (1, 0),
                    (-2, 0) => (-1, 0),
                    // need to move to the right diagonally
                    (2, 1) => (1, 1),
                    (2, -1) => (1, -1),
                    // need to move to the left diagonally
                    (-2, 1) => (-1, 1),
                    (-2, -1) => (-1, -1),
                    // need to move up/down diagonally
                    (1, 2) => (1, 1),
                    (-1, 2) => (-1, 1),
                    (1, -2) => (1, -1),
                    (-1, -2) => (-1, -1),
                    // need to move purely diagonally
                    (2, -2) => (1, -1),
                    (2, 2) => (1, 1),
                    (-2, -2) => (-1, -1),
                    (-2, 2) => (-1, 1),
                    _ => panic!("unhandled case: tail - head = {diff:?}"),
                };

                *second += Point::new(dx, dy);

                if idx == self.knots.len() - 1 {
                    self.tail_visited.insert(self.knots[idx]);
                }
            }
        }
    }
}

pub fn part_02() {
    let movements = INPUT.lines().map(|line| line.parse::<Movement>().unwrap());
    let mut rope = Rope10::new();
    for movement in movements {
        rope.update(movement);
    }
    let visited_count = rope.tail_visited.len();
    println!("{visited_count}");
}
