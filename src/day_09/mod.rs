use std::{
    ops::{Add, AddAssign, Sub},
    str::FromStr,
};

mod part_01;
mod part_02;

pub use part_01::part_01;
pub use part_02::part_02;

use crate::scan;

const INPUT: &str = include_str!("../../inputs/input_09");

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

struct ParseDirectionError;

impl FromStr for Direction {
    type Err = ParseDirectionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "R" => Ok(Self::Right),
            "L" => Ok(Self::Left),
            "U" => Ok(Self::Up),
            "D" => Ok(Self::Down),
            _ => Err(ParseDirectionError),
        }
    }
}

impl Direction {
    pub fn to_point(&self) -> Point {
        match self {
            Direction::Up => Point::new(0, -1),
            Direction::Down => Point::new(0, 1),
            Direction::Left => Point::new(-1, 0),
            Direction::Right => Point::new(1, 0),
        }
    }
}

#[derive(Debug)]
struct Movement {
    direction: Direction,
    repeat: usize,
}

#[derive(Debug)]
struct ParseMovementError;

impl FromStr for Movement {
    type Err = ParseMovementError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let output = scan!(s, char::is_whitespace, Direction, usize);
        Ok(Self {
            direction: output.0.unwrap(),
            repeat: output.1.unwrap(),
        })
    }
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign for Point {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}
