use std::env::args;

mod day_01;
mod day_02;
mod day_03;
mod day_04;
mod day_05;
mod day_06;
mod day_07;
mod day_08;
mod day_09;
mod day_10;
mod util;

fn main() {
    let first = match args().skip(1).next() {
        None => {
            eprintln!(concat!(
                "Didnt't receive any arguments, please pass which ",
                "day you'd like to run as an integer"
            ));
            return;
        }
        Some(s) => s.parse(),
    };

    let num = match first {
        Err(e) => {
            eprintln!("Argument passed wasn't an integer: {e}");
            return;
        }
        Ok(num) => num,
    };

    match num {
        1 => day_01::both_parts(),
        2 => {
            day_02::part_01();
            day_02::part_02();
        }
        3 => {
            day_03::part_01();
            day_03::part_02();
        }
        4 => {
            day_04::part_01();
            day_04::part_02();
        }
        5 => {
            day_05::part_01();
            day_05::part_02();
        }
        6 => {
            day_06::either_part(4);
            day_06::either_part(14);
        }
        7 => {
            day_07::both_parts();
        }
        8 => {
            day_08::part_01();
            day_08::part_02();
        }
        9 => {
            day_09::part_01();
            day_09::part_02();
        }
        10 => {
            day_10::both_parts();
        }
        11..=25 => eprintln!("This day hasn't been implemented yet!"),
        _ => eprintln!("Argument passed doesn't correspond to an advent of code day"),
    }
}
