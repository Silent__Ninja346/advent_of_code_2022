use std::str::FromStr;

const INPUT: &str = include_str!("../inputs/input_08");

#[derive(Debug, Clone, Copy)]
struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }
}

impl From<(usize, usize)> for Point {
    fn from((x, y): (usize, usize)) -> Self {
        Self { x, y }
    }
}

#[derive(Debug)]
struct InputValues {
    width: usize,
    height: usize,
    values: Vec<u32>,
}

impl FromStr for InputValues {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let columns = s.lines().next().ok_or(s.to_string())?.len();
        let rows = s.lines().count();
        Ok(InputValues {
            width: columns,
            height: rows,
            values: s
                .lines()
                .flat_map(|l| l.chars().map(|c| c.to_digit(10)))
                .collect::<Option<Vec<_>>>()
                .ok_or(s.to_string())?,
        })
    }
}

impl InputValues {
    fn is_in_bounds(&self, point: Point) -> bool {
        point.x < self.width && point.y < self.height
    }

    fn get_at_index(&self, point: Point) -> Option<u32> {
        if !self.is_in_bounds(point) {
            None
        } else {
            Some(self.values[point.x + self.width * point.y])
        }
    }
}

const DIRECTIONS: [(isize, isize); 4] = [(-1, 0), (1, 0), (0, -1), (0, 1)];

pub fn part_01() {
    let grid = INPUT.parse::<InputValues>().unwrap();
    let coords = (0..grid.height).into_iter().flat_map(|y| {
        (0..grid.width)
            .into_iter()
            .map(move |x| Point::from((x, y)))
    });

    let visible_trees = coords
        .filter(|point| {
            let tree_height = grid.get_at_index(*point).unwrap();
            DIRECTIONS.iter().any(|(dx, dy)| {
                (1..)
                    .into_iter()
                    .map_while(|i| {
                        let new_x = point.x.checked_add_signed(dx * i)?;
                        let new_y = point.y.checked_add_signed(dy * i)?;
                        grid.get_at_index(Point::new(new_x, new_y))
                    })
                    .all(|height| height < tree_height)
            })
        })
        .count();

    println!("visible trees: {visible_trees}");
}

pub fn part_02() {
    let grid = InputValues::parse_from_str(INPUT);
    let coords = (0..grid.height).into_iter().flat_map(|y| {
        (0..grid.width)
            .into_iter()
            .map(move |x| Point::from((x, y)))
    });

    let highest_score = coords
        .map(|point| {
            let tree_height = grid.get_at_index(&point).unwrap();
            DIRECTIONS
                .iter()
                .map(|(dx, dy)| {
                    let line = (1..).into_iter().map_while(|i| {
                        let new_x = point.x.checked_add_signed(dx * i)?;
                        let new_y = point.y.checked_add_signed(dy * i)?;
                        grid.get_at_index(&Point::new(new_x, new_y))
                    });
                    let mut total = 0;
                    for height in line {
                        total += 1;
                        if height >= tree_height {
                            break;
                        }
                    }
                    total
                })
                .product::<usize>()
        })
        .max()
        .unwrap();

    println!("{highest_score}");
}
